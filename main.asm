%include "lib.inc"
%include "words.inc"
%include "dict.inc"
%define input_size 255


section .rodata
success: db "take your val", 0
not_found: db "value not found", 0
bad_key: db "too long key", 0


section .bss
key_buffer: resb input_size



;section .data
;key_buffer: times input_size db 0

section .text

_start:
    mov rdi, key_buffer
    mov rsi, input_size
    call read_word
    test rax, rax
    je .tooLong
    mov rdi, rax
    mov rsi, first_node
    call find_word
    test rax, rax
    je .noKey
    mov rdi, rax
    call get_value
    mov rdi, rax
    push rax
    mov rdi, success
    call print_string
    call print_newline
    pop rdi
    call print_string
    call print_newline
    jmp .end

    .tooLong:
        mov rdi, bad_key
        call print_err_string
        call print_err_newline
        jmp .end

    .noKey:
        mov rdi, not_found
        call print_err_string
        call print_err_newline

    .end:
    xor rdi, rdi
    call exit

