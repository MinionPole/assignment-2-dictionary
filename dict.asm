%include "colon_params.inc"
%include "lib.inc"
%define shift 8

;rdi - начало ключа, которого мы ищем
;rsi - наше место в ключе
global find_word
find_word:
    .loop:
    cmp rsi, first_node
    je .dont_found
    push rdi
    push rsi
    add rsi, shift
    call string_equals
    pop rsi
    pop rdi
    cmp rax, 1
    je .key_found
    mov rsi, qword[rsi]
    jmp .loop
    
    .dont_found:
        xor rax, rax
        jmp .end        

    .key_found:
        mov rax, rsi

    .end:
        ret


;rdi - начало ключа
;rax - начало значения
global get_value
get_value:
    push rdi
    add rdi, shift
    call string_length
    pop rdi
    add rdi, rax
    add rdi, shift
    inc rdi
    mov rax, rdi
    ret
