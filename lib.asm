section .text


 %define end_of_line 0x0A
 %define zero_symb 0x30
 %define nine_symb 0x39
 %define defis 0x2D
%define EXIT 60
 %define end_s1 0x0020
 %define end_s2 0x0009
 %define end_s3 0x000A
 %define SYS_CALL 0x1
 %define STDERR 0x2
%define STDOUT 0x1
%define STDIN 0x0
%define NULL_TERMINATOR 0x0

global print_err_char
print_err_char:
    push rdi
    mov rax, SYS_CALL
    mov rdi, STDERR
    mov rsi, rsp
    mov rdx, 1
    syscall
    pop rdi
    ret

global print_err_newline
print_err_newline:
    mov rdi, end_of_line
    jmp print_err_char

global print_err_string
print_err_string:
    xor rax, rax
    push rdi
    call string_length
    pop rdi
    mov rdx, rax
    mov rsi, rdi
    mov rax, SYS_CALL
    mov rdi, STDERR
    syscall
    ret

; Принимает код возврата и завершает текущий процесс
global exit
exit: 
    mov rax, EXIT
    syscall

; Принимает указатель на нуль-терминированную строку, возвращает её длину
global string_length
string_length:
    xor rax, rax
    .loop:
        cmp byte[rdi+rax], NULL_TERMINATOR
        je .end
        inc rax
        jmp .loop
    .end:
        ret


; Принимает указатель на нуль-терминированную строку, выводит её в stdout
global print_string
print_string:
    call string_length
    mov rdx, rax
    mov rsi, rdi
    mov rax, SYS_CALL
    mov rdi, STDOUT
    syscall
    ret

; Принимает код символа и выводит его в stdout
global print_char
print_char:
    push rdi
    mov rax, SYS_CALL
    mov rdi, STDOUT
    mov rsi, rsp
    mov rdx, 1
    syscall
    pop rdi
    ret

; Переводит строку (выводит символ с кодом 0xA)
global print_newline
print_newline:
    mov rdi, end_of_line
    jmp print_char


; Выводит беззнаковое 8-байтовое число в десятичном формате 
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
global print_uint
print_uint:

    mov rax, rdi
    mov rcx, 10
    xor r8, r8
    .loop:
        xor rdx, rdx
        div rcx
        test rax, rax
        je .div_ind
        push rdx
        inc r8
        jmp .loop
    .div_ind:
        push rdx;
        inc r8;

    .outLoop:
        pop rdi
        add rdi, zero_symb
        call print_char
        dec r8
        test r8, r8
        je .endIntOut
        jmp .outLoop
        
    .endIntOut:
    ret

; Выводит знаковое 8-байтовое число в десятичном формате
global print_int
print_int:
    cmp rdi, 0
    jge .print_num
    mov r8, rdi
    mov rdi, defis
    call print_char
    mov rdi, r8
    neg rdi

    .print_num:
    jmp print_uint

; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
global string_equals
string_equals:
    ; rsi и rdi
    xor rcx, rcx
    .loop:
        mov r9b, byte[rsi + rcx]
        mov r10b, byte[rdi + rcx]
        cmp r9b, r10b
        jne .notEq
        test r9b, r9b
        je .equal
        inc rcx
        jmp .loop
    .equal:
            xor rax, rax
            mov rax, 1
            jmp .endEq
    .notEq:
            xor rax, rax
    .endEq:

    ret

; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
global read_char
read_char:
    push 0
    xor rax, rax
    mov rdi, STDIN
    mov rsi, rsp
    mov rdx, 1
    syscall
    pop rax
    ret 

; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор
; rdi
; rsi
global read_word
read_word:
    push rdi
    push rsi
    .skipWhite:
        call read_char

        cmp rax, end_s1
        je .skipWhite
        cmp rax, end_s2
        je .skipWhite
        cmp rax, end_s3
        je .skipWhite

    pop r8 ;rsi
    pop r9 ;rdi
    xor r10, r10
    .readingLoop:
        test rax, rax
        je .endLoop
        cmp rax, end_s1
        je .endLoop
        cmp rax, end_s2
        je .endLoop
        cmp rax, end_s3
        je .endLoop
        
        mov byte[r9 + r10], al
        inc r10
        cmp r10, r8
        jge .overflow

        call read_char

        jmp .readingLoop

    .endLoop:
        mov byte[r9 + r10], NULL_TERMINATOR
        jmp .end
    

    .overflow:
        xor r9, r9
        xor r10, r10
        jmp .end

    .end:
        mov rax, r9
        mov rdx, r10

    ret
 

; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
global parse_uint
parse_uint:
    xor rax, rax
    xor r8, r8; счётчик считаного
    ;считывание цифр
    .Reading:
        mov al, byte[rdi + r8]
        cmp al, zero_symb
        jl .endReading
        cmp al, nine_symb
        jg .endReading
        sub rax, zero_symb
        push rax
        inc r8
        jmp .Reading

    .endReading:

    ;расшифровк(думаю что это относилось к комментариям к регистрам)
    xor rsi, rsi ;сумма
    xor rdi, rdi ; длина
    mov r9, 10 ; Основание
    mov r10, 1 ; разряд
    .deCrypt:
        test r8, r8
        je .endDecrypt
        dec r8
        inc rdi
        pop r11
        mov rax, r11
        mul r10
        add rsi, rax
        mov rax, r10
        mul r9
        mov r10, rax
        xor rax, rax
        jmp .deCrypt


    .endDecrypt:
        mov rax, rsi
        mov rdx, rdi

    ret

; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был) 
; rdx = 0 если число прочитать не удалось
global parse_int
parse_int:
    xor rax, rax
    push rdi

    cmp byte[rdi], defis
    jne .readUns;
    inc rdi

    .readUns:
        push rdi
        call parse_uint
        pop rdi
        pop r8

        test rax, rax
        je .end
        
        test rdx, rdx
        je .end

        cmp rdi, r8
        je .end

        neg rax
        inc rdx

    .end:



    ret 

; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
; rdi, rsi, rdx
global string_copy
string_copy:
    xor rax, rax
    xor r8, r8
    .loop:
        cmp r8, rdx
        jge .over
        mov al, byte[rdi + r8]
        mov byte[rsi + r8], al
        test rax, rax
        je .end
        inc r8
        jmp .loop

    .over:
        xor r8, r8

    .end:
        mov rax, r8
    
    ret