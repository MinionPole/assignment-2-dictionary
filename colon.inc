%include "colon_params.inc"

%macro colon 2

%2: dq first_node ; pointer to the first element
db %1, 0 ; key
%define first_node %2

%endmacro