ASM=nasm
ASMFLAGS=-f elf64
LD=ld
FILES=main.o dict.o lib.o

program: $(FILES)
	$(LD) -o $@ $^

.PHONY: clean
clean:
	$(RM) *.o

colon.inc: colon_params.inc
	touch $@

words.inc: colon.inc
	touch $@

dict.asm: lib.inc colon_params.inc
	touch $@

main.asm: lib.inc dict.inc words.inc
	touch $@

%.o: %.asm
	$(ASM) $(ASMFLAGS) -o $@ $<